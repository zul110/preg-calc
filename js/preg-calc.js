const pregDaySelector = $('#preg-calc-day-selector');
const pregMonthSelector = $('#preg-calc-month-selector');
const pregYearSelector = $('#preg-calc-year-selector');

const pregResultDay = $('#preg-calc-day-container');
const pregResultDate = $('#preg-calc-date-container');
const pregResultWeek = $('#preg-calc-weeks');
const pregResultWeeksTitleContainer = $('#preg-calc-weeks-title-container');

const pregFirstTrimContainer = $('#preg-calc-first-trim-container .preg-calc-trim-content-container');
const pregSecondTrimContainer = $('#preg-calc-second-trim-container .preg-calc-trim-content-container');
const pregThirdTrimContainer = $('#preg-calc-third-trim-container .preg-calc-trim-content-container');

const pregSubmitButton = $('#btn_Submit_PregCalc_Right');
const pregResultButton = $('#preg-calc-result-return-button');

const pregCalcInit = () => {
    pregDaySelector.val(0).change();
    pregMonthSelector.val(0).change();
    pregYearSelector.val(0).change();

    pregMonthSelector.prop('disabled', true);
    pregDaySelector.prop('disabled', true);

    const pregUpdateYears = () => {
        const today = moment().format('YYYY');
        const lastYear = moment().subtract(1, 'year').format('YYYY');

        const years = [today, lastYear];

        pregYearSelector.innerHTML = years.forEach(year => pregYearSelector.append(new Option(year, year)));
    };

    const pregUpdateMonths = () => {
        const months = [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
        ];

        pregMonthSelector.innerHTML = months.forEach(month => pregMonthSelector.append(new Option(moment(month, 'M').format('MMMM'), month)));
    };

    const pregUpdateDays = () => {
        const days = [];
        const numDaysInMonth = moment(`${pregYearSelector.val()}-${pregMonthSelector.val()}`, 'YYYY-MM').daysInMonth();

        let day = 1;
        while (day <= numDaysInMonth) {
            days.push(day++);
        }

        pregDaySelector.innerHTML = days.forEach(day => pregDaySelector.append(new Option(day, day)));
    };

    pregUpdateYears();
    pregUpdateMonths();

    pregYearSelector.on('change', e => {
        const year = e.target.value;
        const month = pregMonthSelector.val();

        let _year = year;

        pregMonthSelector.prop('disabled', year == 0);
        pregDaySelector.prop('disabled', year == 0 || month == 0);
    });

    pregMonthSelector.on('change', e => {
        const year = pregYearSelector.val();
        const month = e.target.value;

        let _month = month;

        pregUpdateDays();

        pregDaySelector.prop('disabled', year == 0 || month == 0);
    });

    pregDaySelector.on('change', e => {
        const day = pregDaySelector.val();

        let _day = day;
    });
};

pregSubmitButton.click(() => {
    const year = pregYearSelector.val();
    const month = pregMonthSelector.val();
    const day = pregDaySelector.val();

    if ((!year || !month || !day) || (year == 0 || month == 0 || day == 0)) {
        pregHideResults();
    } else {
        pregShowResults();

        // Get LMP
        const lmp = moment(`${year}-${month}-${day}`, 'YYYY-M-D');

        // Add 7 days
        const lmpPlus7Days = lmp.add(7, 'days');

        // Subtract 3 months
        const subtract3Months = lmpPlus7Days.subtract(3, 'months');

        // Add year
        const estimatedDueDate = subtract3Months.add(1, 'year').format('DD/MM/YYYY');

        const pregWeeks = Math.round(moment.duration(moment().diff(moment(`${year}-${month}-${day}`))).asDays() / 7);
        const pregDays = Math.round(moment.duration(moment().diff(moment(`${year}-${month}-${day}`))).asDays() % 7);

        const weeks = `الأسبوع ${pregWeeks}${pregDays > 0 && pregDays < 7 ? `, اليوم ${pregDays}` : ''}`;

        pregResultDay.html(moment(estimatedDueDate, 'DD/MM/YYYY').format('dddd'));
        pregResultDate.html(estimatedDueDate);

        if(pregWeeks <= 41) {
            pregResultWeeksTitleContainer.html('أسبوع الحمل الحالي:');
            pregResultWeek.html(weeks);
        } else {
            pregResultWeeksTitleContainer.html('');
            pregResultWeek.html('');
        }

        const trimFormat = 'DD MMM YYYY';

        const firstTrimStart = moment(`${year}-${month}-${day}`, 'YYYY-M-D');
        const firstTrimStartFormat = firstTrimStart.format(trimFormat);
        const firstTrimEnd = firstTrimStart.add(13, 'weeks').subtract(1, 'day');
        const firstTrimEndFormat = firstTrimEnd.format(trimFormat);
        const firstTrim = `${firstTrimStartFormat} - ${firstTrimEndFormat}`;

        const secondTrimStart = firstTrimEnd.add(1, 'day');
        const secondTrimStartFormat = secondTrimStart.format(trimFormat);
        const secondTrimEnd = secondTrimStart.add(13, 'weeks').subtract(1, 'day');
        const secondTrimEndFormat = secondTrimEnd.format(trimFormat);
        const secondTrim = `${secondTrimStartFormat} - ${secondTrimEndFormat}`;

        const thirdTrimStart = secondTrimEnd.add(1, 'day');
        const thirdTrimStartFormat = thirdTrimStart.format(trimFormat);
        const thirdTrimEnd = thirdTrimStart.add(13, 'weeks').subtract(1, 'day');
        const thirdTrimEndFormat = moment(estimatedDueDate, 'DD/MM/YYYY').format(trimFormat);
        const thirdTrim = `${thirdTrimStartFormat} - ${thirdTrimEndFormat}`;

        pregFirstTrimContainer.html(firstTrim);
        pregSecondTrimContainer.html(secondTrim);
        pregThirdTrimContainer.html(thirdTrim);
    }
});

pregResultButton.click(() => {
    pregHideResults();
});

const pregHideResults = () => {
    $('.preg-calc-result').css('display', 'none');
    $('.calcWzDescCnts').css('display', 'block');
};

const pregShowResults = () => {
    $('.preg-calc-result').css('display', 'flex');
    $('.calcWzDescCnts').css('display', 'none');
};